//
//  ShopingList_ModuleTCAApp.swift
//  ShopingList_ModuleTCA
//
//  Created by Илья Шаповалов on 16.05.2023.
//

import SwiftUI

@main
struct ShopingList_ModuleTCAApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
