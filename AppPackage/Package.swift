// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AppPackage",
    platforms: [
        .iOS(.v14),
        .macOS(.v11)
    ],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
//        .library(
//            name: "AppPackage",
//            targets: ["AppPackage"]),
        .library(name: "Models", targets: ["Models"]),
        .library(name: "ShoppingListFeature", targets: ["ShoppingListFeature"]),
        .library(name: "ProductFeature", targets: ["ProductFeature"]),
    ],
    dependencies: [
        .package(url: "https://github.com/pointfreeco/swift-composable-architecture", from: "0.51.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
//        .target(
//            name: "AppPackage"),
        .target(name: "Models"),
        .target(
            name: "ShoppingListFeature",
            dependencies: [
                "Models",
                "ProductFeature",
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture")
            ]),
        .target(
            name: "ProductFeature",
            dependencies: [
                "Models",
                .product(name: "ComposableArchitecture", package: "swift-composable-architecture")
            ]),
//        .testTarget(
//            name: "AppPackageTests",
//            dependencies: ["AppPackage"]),
    ]
)
