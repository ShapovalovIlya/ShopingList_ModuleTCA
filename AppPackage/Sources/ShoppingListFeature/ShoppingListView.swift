//
//  SwiftUIView.swift
//  
//
//  Created by Илья Шаповалов on 16.05.2023.
//

import SwiftUI
import ComposableArchitecture
import ProductFeature

public struct ShoppingListView: View {
    let store: StoreOf<ShoppingListDomain>
    
    public var body: some View {
        WithViewStore(store, observe: { $0 }) { viewStore in
            NavigationView {
                List {
                    ForEachStore(
                        store.scope(
                            state: \.products,
                            action: ShoppingListDomain.Action.product),
                        content: ProductView.init)
                }
                .navigationTitle("Shopping list")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Add") {
                            viewStore.send(.addProduct)
                        }
                    }
                }
            }
        }
    }
}

struct ShoppingListView_Previews: PreviewProvider {
    static var previews: some View {
        ShoppingListView(store: ShoppingListDomain.testStore)
    }
}
