//
//  File.swift
//  
//
//  Created by Илья Шаповалов on 16.05.2023.
//

import Foundation
import ComposableArchitecture
import Models
import ProductFeature

public struct ShoppingListDomain: ReducerProtocol {
    //MARK: - State
    public struct State: Equatable {
        var products: IdentifiedArrayOf<ProductDomain.State> = .init()
    }
    
    //MARK: - Action
    public enum Action: Equatable {
        case addProduct
        case product(id: ProductDomain.State.ID, action: ProductDomain.Action)
    }
    
    //MARK: - Environment
    @Dependency(\.uuid) var uuid
    
    //MARK: - Reducer
    public var body: some ReducerProtocol<State, Action> {
        Reduce { state, action in
            switch action {
            case .addProduct:
                state.products.insert(
                    ProductDomain.State(id: uuid.callAsFunction()),
                    at: 0)
            case let .product(id, action):
                break
            }
            return .none
        }
        .forEach(\.products, action: /Action.product(id:action:), element: ProductDomain.init)
    }
    
    //MARK: - Live environment
    static let live = Self()
    
    //MARK: - Test store
    static let testStore = Store(
        initialState: ShoppingListDomain.State(),
        reducer: ShoppingListDomain())
}
