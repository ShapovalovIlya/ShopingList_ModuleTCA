//
//  File.swift
//  
//
//  Created by Илья Шаповалов on 16.05.2023.
//

import Foundation
import ComposableArchitecture

public struct ProductDomain: ReducerProtocol {
    //MARK: - State
    public struct State: Equatable, Identifiable {
        public var id: UUID
        @BindingState public var name: String
        public var inbox: Bool
        
        public init(
            id: UUID,
            name: String = .init(),
            inbox: Bool = false
        ) {
            self.id = id
            self.name = name
            self.inbox = inbox
        }
    }
    
    //MARK: - Action
    public enum Action: Equatable, BindableAction {
        case toggleStatus
        case binding(BindingAction<State>)
    }
    
    //MARK: - Environment
    
    //MARK: - Reducer
    public var body: some ReducerProtocol<State, Action> {
        BindingReducer()
        Reduce { state, action in
            switch action {
            case .toggleStatus:
                state.inbox.toggle()
            case .binding:
                break
            }
            return .none
        }
    }
    
    public init() {}
    
    //MARK: - Live environment
    static let live = Self()
    
    //MARK: - Test store
    static let testStore = Store(
        initialState: ProductDomain.State(id: UUID()),
        reducer: ProductDomain())
}
