//
//  SwiftUIView.swift
//  
//
//  Created by Илья Шаповалов on 16.05.2023.
//

import SwiftUI
import ComposableArchitecture

public struct ProductView: View {
    let store: StoreOf<ProductDomain>
    
    public var body: some View {
        WithViewStore(store, observe: { $0 }) { viewStore in
            HStack {
                Button {
                    viewStore.send(.toggleStatus)
                } label: {
                    Image(
                        systemName: viewStore.inbox
                        ? "checkmark.square"
                        : "square")
                }
                .buttonStyle(.plain)
                TextField("New product", text: viewStore.binding(\.$name))
                    .disabled(viewStore.inbox)
            }
            .foregroundColor(viewStore.inbox ? .gray : nil)
        }
    }
    
    public init(
        store: StoreOf<ProductDomain>
    ) {
        self.store = store
    }
}

struct ProductView_Previews: PreviewProvider {
    static var previews: some View {
        ProductView(store: ProductDomain.testStore)
    }
}
